import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {AddToCart, removeFromCart} from '../actions/addToCartAction';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';

function CartScreen(props) {
  const productId = props.match.params.id;
  const qty = props.location.search
    ? Number(props.location.search.split('=')[1])
    : 1;
  const cart = useSelector((state) => state.cart);
  const {cartItems} = cart;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(AddToCart(productId, qty));
    return () => {
      //
    };
  }, []);

  const removeFromCartHandler = (productId) => {
    dispatch(removeFromCart(productId));
  };

  const checkouthandler = () => {
    props.history.push('/signin?redirect=shipping');
  };

  return (
    <div className="cart">
      <div className="cart-list">
        <ul className="cart-list-container">
          <li>
            <h3>Shopping Cart</h3>
            <div>Price</div>
          </li>
          {cartItems.length == 0 ? (
            <div>Cart is empty</div>
          ) : (
            cartItems.map((item) => (
              <li>
                <div className="cart-image">
                  {' '}
                  <img src={item.image} alt="image" />
                </div>

                <div className="cart-name">
                  <div>
                    {' '}
                    <Link to={'/products/'}>{item.name}</Link>
                  </div>
                  <div>
                    Qty:
                    <select
                      value={item.qty}
                      onChange={(e) =>
                        dispatch(AddToCart(item.product, e.target.value))
                      }>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                    <button
                      type="button"
                      onClick={() => removeFromCartHandler(item.product)}
                      className="button">
                      DELETE
                    </button>
                  </div>
                </div>
                <div className="cart-price">Rs.{item.price}</div>
              </li>
            ))
          )}
        </ul>
      </div>
      <div className="cart-action">
        <h3>
          Subtotal({cartItems.reduce((a, c) => a + c.qty, 0)} items) : Rs.
          {cartItems.reduce((a, c) => a + c.price * c.qty, 0)}
        </h3>
        <button
          className="button primary full-width"
          disabled={cartItems.length === 0}
          onClick={checkouthandler}>
          Proceed to checkout
        </button>
      </div>
    </div>
  );
}

export default CartScreen;
