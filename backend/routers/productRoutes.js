import express from 'express';
import Product from '../Models/productModel';
import {isAdmin, isAuth} from '../util';

const router = express.Router();

router.get('/', async (req, res) => {
  const products = await Product.find({});
  res.send(products);
});

router.get('/:id', async (req, res) => {
  const products = await Product.findOne({_id: req.params.id});
  if (products) {
    res.send(products);
  }
  res.status(400).send({msg: 'Product not found'});
});

router.post('/', async (req, res) => {
  const product = new Product({
    name: req.body.name,
    image: req.body.image,
    brand: req.body.brand,
    price: req.body.price,
    numReviews: req.body.numReviews,
    rating: req.body.rating,
    category: req.body.category,
    stockInCount: req.body.stockInCount,
    description: req.body.description,
  });

  const newProduct = await product.save();
  if (newProduct) {
    return res.status(204).send({msg: 'product Created', data: newProduct});
  }
  return res.status(500).send({msg: 'errorn iN creating product'});
});

router.put('/:id', isAuth, isAdmin, async (req, res) => {
  const productId = req.params.id;
  const product = await Product.findById(productId);
  if (product) {
    product.name = req.body.name;
    product.image = req.body.image;
    product.price = req.body.price;
    product.category = req.body.category;
    product.stockInCount = req.body.stockInCount;
    product.description = req.body.description;
    product.brand = req.body.brand;

    const newProduct = await product.save();
    if (newProduct) {
      return res.status(200).send({msg: 'product Created', data: newProduct});
    }
  }
  return res.status(500).send({msg: 'errorn iN creating product'});
});

router.delete('/:id', isAuth, isAdmin, async (req, res) => {
  const deletedProduct = await Product.findById(req.params.id);
  if (deletedProduct) {
    await deletedProduct.remove();
    res.send({message: 'Product Deleted'});
  } else {
    res.send('Error in Deletion.');
  }
});

export default router;
