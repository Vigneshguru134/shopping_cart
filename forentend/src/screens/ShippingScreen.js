import React from 'react';
import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Link} from 'react-router-dom';
import CheckoutSteps from '../components/CheckoutSteps';

import {saveShipping} from '../actions/addToCartAction';

export default function ShippingScreen(props) {
  const [address, setAddress] = useState('');
  const [postalCode, setPostalcode] = useState('');
  const [city, setCity] = useState('');
  const [country, setCountry] = useState('');
  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(saveShipping({address, postalCode, city, country}));
    props.history.push('/payment');
  };

  return (
    <div>
      <CheckoutSteps step1 step2></CheckoutSteps>
      <div className="form">
        <form onSubmit={submitHandler}>
          <ul className="form-container">
            <li>
              <h2>Shipping</h2>
            </li>

            <li>
              <label htmlFor="address">Address:</label>
              <input
                type="text"
                name="address"
                id="address"
                onChange={(e) => setAddress(e.target.value)}></input>
            </li>

            <li>
              <label htmlFor="postalcode">PostalCode:</label>
              <input
                type="text"
                name="postalcode"
                id="postalCode"
                onChange={(e) => setPostalcode(e.target.value)}></input>
            </li>
            <li>
              <label htmlFor="city">City:</label>
              <input
                type="text"
                name="city"
                id="city"
                onChange={(e) => setCity(e.target.value)}></input>
            </li>
            <li>
              <label htmlFor="country">Country:</label>
              <input
                type="text"
                name="country"
                id="country"
                onChange={(e) => setCountry(e.target.value)}></input>
            </li>

            <button type="submit" className="button primary text-center">
              Register
            </button>
          </ul>
        </form>
      </div>
    </div>
  );
}
