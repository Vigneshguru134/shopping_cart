import {
  PRODUCT_LIST_FAILURE,
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_DETAILS_REQUEST,
  PRODUCT_DETAILS_SUCCESS,
  PRODUCT_DETAILS_FAILURE,
  PRODUCT_SAVE_REQUEST,
  PRODUCT_SAVE_SUCCESS,
  PRODUCT_SAVE_FAILURE,
  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAILURE,
} from '../constants/productListConstants';
import axios from 'axios';

const ListProducts = () => async (dispatch) => {
  try {
    dispatch({type: PRODUCT_LIST_REQUEST});
    const {data} = await axios.get('/api/products');
    dispatch({type: PRODUCT_LIST_SUCCESS, payload: data});
  } catch (error) {
    dispatch({type: PRODUCT_LIST_FAILURE, payload: error});
  }
};

const saveProduct = (product) => async (dispatch, getState) => {
  console.log(product);
  try {
    dispatch({type: PRODUCT_SAVE_REQUEST, payload: product});
    const {
      userSignin: {userInfo},
    } = getState();
    if (!product._id) {
      const {data} = await axios.post('/api/products', product, {
        headers: {
          Authorization: 'vignesh' + userInfo.token,
        },
      });
      dispatch({type: PRODUCT_SAVE_SUCCESS, payload: data});
    } else {
      const {data} = await axios.put('/api/products/' + product._id, product, {
        headers: {
          Authorization: 'vignesh' + userInfo.token,
        },
      });
      dispatch({type: PRODUCT_SAVE_SUCCESS, payload: data});
    }
  } catch (error) {
    dispatch({type: PRODUCT_SAVE_FAILURE, payload: error.message});
  }
};

const deleteProduct = (productId) => async (dispatch, getState) => {
  try {
    const {
      userSignin: {userInfo},
    } = getState();
    dispatch({type: PRODUCT_DELETE_REQUEST, payload: productId});
    const {data} = await axios.delete('/api/products/' + productId, {
      headers: {
        Authorization: 'vignesh' + userInfo.token,
      },
    });
    dispatch({type: PRODUCT_DELETE_SUCCESS, payload: data, success: true});
  } catch (error) {
    dispatch({type: PRODUCT_DELETE_FAILURE, payload: error.message});
  }
};
const detailsProduct = (productId) => async (dispatch) => {
  try {
    dispatch({type: PRODUCT_DETAILS_REQUEST, payload: productId});

    const {data} = await axios('/api/products/' + productId);
    dispatch({type: PRODUCT_DETAILS_SUCCESS, payload: data});
  } catch (error) {
    dispatch({type: PRODUCT_DETAILS_FAILURE, payload: error.message});
  }
};

export {ListProducts, detailsProduct, saveProduct, deleteProduct};
