import {
  ADD_TO_CART,
  REMOVE_CART_ITEM,
  CART_SAVE_SHIPPING,
  CART_SAVE_PAYMENT,
} from '../constants/addToScreenConstants';
import axios from 'axios';
import Cookie from 'js-cookie';

const AddToCart = (productId, qty) => async (dispatch, getState) => {
  const {data} = await axios.get('/api/products/' + productId);
  console.log(`data`, data);

  dispatch({
    type: ADD_TO_CART,
    payload: {
      product: data._id,
      name: data.name,
      image: data.image,
      price: data.price,
      stockInCount: data.stockInCount,
      qty,
    },
  });
  const {
    cart: {cartItems},
  } = getState();
  Cookie.set('cartItems', JSON.stringify(cartItems));
};

const removeFromCart = (productId) => async (dispatch, getState) => {
  dispatch({type: REMOVE_CART_ITEM, payload: productId});
  const {
    cart: {cartItems},
  } = getState();
  Cookie.set('cartItems', JSON.stringify(cartItems));
};

const saveShipping = (data) => (dispatch) => {
  dispatch({type: CART_SAVE_SHIPPING, payload: data});
};
const savePayment = (data) => (dispatch) => {
  dispatch({type: CART_SAVE_PAYMENT, payload: data});
};
export {AddToCart, removeFromCart, saveShipping, savePayment};
