import {
  SIGNIN_REGISTER_FAIL,
  SIGNIN_REGISTER_REQEST,
  SIGNIN_REGISTER_SUCCESS,
  SIGNIN_USER_FAIL,
  SIGNIN_USER_REQEST,
  SIGNIN_USER_SUCCESS,
  SIGNIN_SAVE_SHIPPING,
} from '../constants/signinConstants';
import Cookie from 'js-cookie';
import axios from 'axios';

const signin = (email, password) => async (dispatch) => {
  try {
    dispatch({type: SIGNIN_USER_REQEST, payload: {email, password}});
    const {data} = await axios.post('/api/users/signin', {email, password});
    dispatch({type: SIGNIN_USER_SUCCESS, payload: data});
    Cookie.set('userInfo', JSON.stringify(data));
  } catch (error) {
    dispatch({type: SIGNIN_USER_FAIL, payload: error.message});
  }
};

const register = (name, email, password) => async (dispatch) => {
  try {
    dispatch({type: SIGNIN_REGISTER_REQEST, payload: {name, email, password}});
    const {data} = await axios.post('/api/users/register', {
      name,
      email,
      password,
    });
    dispatch({type: SIGNIN_REGISTER_SUCCESS, payload: data});
    Cookie.set('userInfo', JSON.stringify(data));
  } catch (error) {
    dispatch({type: SIGNIN_REGISTER_FAIL, payload: error.message});
  }
};

export {signin, register};
