import React from 'react';
import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Link} from 'react-router-dom';
import {signin} from '../actions/signinAction';

export default function SigninScreen(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const userSignin = useSelector((state) => state.userSignin);
  const {loading, userInfo, error} = userSignin;
  const redirect = props.location.search
    ? props.location.search.split('=')[1]
    : '/';
  const dispatch = useDispatch();
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(email, password));
  };
  useEffect(() => {
    if (userInfo) {
      props.history.push(redirect);
    }
    return () => {
      //
    };
  }, [userInfo]);

  return (
    <div className="form">
      <form onSubmit={submitHandler}>
        <ul className="form-container">
          <li>
            <h2>SignIn</h2>
          </li>
          <li>
            {loading && <div>loading...</div>}
            {error && <div>{error}</div>}
          </li>
          <li>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              name="email"
              id="email"
              onChange={(e) => setEmail(e.target.value)}></input>
          </li>
          <li>
            <label htmlFor="password">Password:</label>
            <input
              type="password"
              name="password"
              id="password"
              onChange={(e) => setPassword(e.target.value)}></input>
          </li>
          <button type="submit" className="button primary text-center">
            SignIn
          </button>
          <li>New to amozona?</li>
          <li>
            {' '}
            <Link
              className="button secondary text-center"
              to={
                redirect === '/' ? 'register' : 'register?redirect=' + redirect
              }>
              {' '}
              Create your amazona account
            </Link>{' '}
          </li>
        </ul>
      </form>
    </div>
  );
}
