import React from 'react';
import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Link} from 'react-router-dom';
import CheckoutSteps from '../components/CheckoutSteps';

import {savePayment} from '../actions/addToCartAction';

export default function ShippingScreen(props) {
  const [paymentMethod, setPaymentMehod] = useState('');

  const dispatch = useDispatch();

  const submitHandler = (e) => {
    console.log('xqwd');
    e.preventDefault();
    dispatch(savePayment({paymentMethod}));
    props.history.push('/placeorder');
  };

  return (
    <div>
      <CheckoutSteps step1 step2 step3></CheckoutSteps>
      <div className="form">
        <form onSubmit={submitHandler}>
          <ul className="form-container">
            <li>
              <h2>Payment</h2>
            </li>

            <li>
              <div>
                <input
                  type="radio"
                  name="payment"
                  id="payment"
                  value="paypal"
                  onChange={(e) => setPaymentMehod(e.target.value)}></input>
                <label htmlFor="payment">Paypal</label>
              </div>
            </li>

            <button type="submit" className="button primary text-center">
              proceed to payment
            </button>
          </ul>
        </form>
      </div>
    </div>
  );
}
