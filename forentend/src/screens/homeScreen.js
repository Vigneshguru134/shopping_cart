import React, {useState, useEffect} from 'react';
import data from '../data';
import {Link} from 'react-router-dom';
import axios from 'axios';
import {ListProducts} from '../actions/productListActions';
import {useSelector, useDispatch} from 'react-redux';

function HomeScreen() {
  const productList = useSelector((state) => state.productList);
  const {loading, products, error} = productList;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(ListProducts());
    return () => {
      //
    };
  }, []);

  return loading ? (
    <div>loading....</div>
  ) : error ? (
    <div>{error}</div>
  ) : (
    <ul className="product-list">
      {products.map((product) => (
        <li>
          <Link to={'/productPage/' + product._id}>
            <img classsName="product-image" src={product.image}></img>
          </Link>
          <Link to={'/productPage/' + product._id}>
            <div className="product-name">{product.name}</div>
          </Link>
          <div className="product-brand">Brand:{product.brand}</div>
          <div className="product-price">Price:{product.price}</div>
          <div className="product-rating">Rating:{product.rating}</div>
        </li>
      ))}
    </ul>
  );
}

export default HomeScreen;
