import {
  SIGNIN_REGISTER_FAIL,
  SIGNIN_REGISTER_REQEST,
  SIGNIN_REGISTER_SUCCESS,
  SIGNIN_USER_FAIL,
  SIGNIN_USER_REQEST,
  SIGNIN_USER_SUCCESS,
} from '../constants/signinConstants';

function signinReducer(state = {userInfo: {}}, action) {
  switch (action.type) {
    case SIGNIN_USER_REQEST:
      return {loading: true};
    case SIGNIN_USER_SUCCESS:
      return {loading: false, userInfo: action.payload};
    case SIGNIN_USER_FAIL:
      return {loading: false, error: action.payload};
    default:
      return state;
  }
}
function registerReducer(state = {}, action) {
  switch (action.type) {
    case SIGNIN_REGISTER_REQEST:
      return {loading: true};
    case SIGNIN_REGISTER_SUCCESS:
      return {loading: false, userInfo: action.payload};
    case SIGNIN_REGISTER_FAIL:
      return {loading: false, error: action.payload};
    default:
      return state;
  }
}

export {signinReducer, registerReducer};
