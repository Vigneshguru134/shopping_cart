import React from 'react';
import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Link} from 'react-router-dom';
import {register} from '../actions/signinAction';

export default function RegisterScreen(props) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repassword, setRepassword] = useState('');
  const registerUser = useSelector((state) => state.registerUser);
  const {loading, userInfo, error} = registerUser;
  const dispatch = useDispatch();
  const redirect = props.location.search
    ? props.location.search.split('=')[1]
    : '/';
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(register(name, email, password));
  };
  useEffect(() => {
    if (userInfo) {
      props.history.push(redirect);
    }
    return () => {
      //
    };
  }, [userInfo]);

  return (
    <div className="form">
      <form onSubmit={submitHandler}>
        <ul className="form-container">
          <li>
            <h2>Register</h2>
          </li>
          <li>
            {loading && <div>loading...</div>}
            {error && <div>{error}</div>}
          </li>
          <li>
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              name="name"
              id="name"
              onChange={(e) => setName(e.target.value)}></input>
          </li>
          <li>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              name="email"
              id="email"
              onChange={(e) => setEmail(e.target.value)}></input>
          </li>
          <li>
            <label htmlFor="password">Password:</label>
            <input
              type="password"
              name="password"
              id="password"
              onChange={(e) => setPassword(e.target.value)}></input>
          </li>
          <li>
            <label htmlFor="repassword">Re-Password:</label>
            <input
              type="password"
              name="repassword"
              id="repassword"
              onChange={(e) => setRepassword(e.target.value)}></input>
          </li>
          <button type="submit" className="button primary text-center">
            Register
          </button>
          <li>
            <div style={{textAlign: 'center', justifyContent: 'space-around'}}>
              Already have account?
              <Link
                to={
                  redirect === '/' ? 'signin' : 'signin?redirect=' + redirect
                }>
                Sign-In
              </Link>
            </div>
          </li>
        </ul>
      </form>
    </div>
  );
}
