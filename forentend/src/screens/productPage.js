import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {detailsProduct} from '../actions/productListActions';

export default function ProductPage(props) {
  const [qty, setQty] = useState(1);
  const productDetails = useSelector((state) => state.productDetails);
  const {product, loading, error} = productDetails;
  const dispatch = useDispatch();

  const handleAddToCart = () => {
    props.history.push('/cart/' + props.match.params.id + '?qty=' + qty);
  };

  useEffect(() => {
    dispatch(detailsProduct(props.match.params.id));
    return () => {
      //
    };
  }, []);

  return (
    <div>
      <div className="back-to-homescreen">
        {' '}
        <Link to="/"> Back to HomeScreen</Link>
      </div>{' '}
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (
        <div class="details">
          <div class="details-image">
            <img src={product.image} alt="image"></img>
          </div>
          <div class="details-info">
            <ul>
              <li>
                <h4>{product.name}</h4>
              </li>
              <li>
                Ratings:{product.rating}Stars ({product.numReviews} Reviews)
              </li>
              <li>Price:₹{product.price}</li>
              <li>Description:{product.description}</li>
            </ul>
          </div>
          <div className="details-action">
            <ul>
              <li>Price:₹{product.price}</li>
              <li>
                Status:{product.stockInCount > 0 ? 'In Stock' : 'Out of Stock'}
              </li>
              <li>
                Qty:
                <select
                  value={qty}
                  onChange={(e) => {
                    setQty(e.target.value);
                  }}>
                  {[...Array(product.stockInCount).keys()].map((x) => (
                    <option value={x + 1} keys={x + 1}>
                      {x + 1}
                    </option>
                  ))}
                </select>
              </li>
              <li>
                {product.stockInCount > 0 && (
                  <button className="button primary" onClick={handleAddToCart}>
                    Add to Cart
                  </button>
                )}
              </li>
            </ul>
          </div>
        </div>
      )}
    </div>
  );
}
