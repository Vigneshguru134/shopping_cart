import express from 'express';
import data from './data';
import dotenv from 'dotenv';
import config from './config';
import userRoute from './routers/UserRoutes';
import Mongoose from 'mongoose';
import bodyparser from 'body-parser';
import productRoute from './routers/productRoutes';

dotenv.config();

const mongoUrl = config.MONGODB_URL;

Mongoose.connect(mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
})
  .then(() => {
    console.log('Database connected sucessfully');
  })
  .catch((error) => console.log(error.reason));

const app = express();
app.use(bodyparser.json());
app.use('/api/users', userRoute);
app.use('/api/products', productRoute);
app.get('/api/products', (req, res) => {
  res.send(data.products);
});

// app.get('/api/products/:id', (req, res) => {
//   const productId = req.params.id;
//   const product = res.send(data.products.find((x) => x._id == productId));

//   if (product) res.send(product);
//   else res.status(404).send({msg: 'No product found'});
// });

app.listen(5000, () => {
  console.log('App listening on the port 5000');
});
