import React from 'react';
import homeScreen from './screens/homeScreen';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import productPage from './screens/productPage';
import CartScreen from './screens/CartScreen';
import signinScreen from './screens/signinScreen';
import {useSelector} from 'react-redux';
import RegisterScreen from './screens/registerScreen';
import ProductScreen from './screens/ProductsScreen';
import ShippingScreen from './screens/ShippingScreen';
import PaymentMethod from './screens/PaymentMethod';
import PlaceorderScreen from './screens/placeorderScreen';

export default function App() {
  const userSignin = useSelector((state) => state.userSignin);
  const {userInfo} = userSignin;

  const openMenu = () => {
    document.querySelector('.sidebar').classList.add('open');
  };
  const closeMennu = () => {
    document.querySelector('.sidebar').classList.remove('open');
  };

  return (
    <Router>
      <div className="grid-container">
        <header className="header">
          <div className="title-name">
            <button className="aside-button" onClick={openMenu}>
              &#9776;
            </button>
            <Link to="/">Shopping Market</Link>
          </div>
          <div className="header-links">
            <a href="cart.html">Cart</a>
            {userInfo ? (
              <Link to="/profile">{userInfo.name}</Link>
            ) : (
              <Link to="/signin">SignIn</Link>
            )}
          </div>
        </header>
        <aside className="sidebar">
          <h3>Shopping Items</h3>
          <button className="close-button-sidebar" onClick={closeMennu}>
            X
          </button>
          <ul>
            <li>Pants</li>
            <li>shirts</li>
          </ul>
        </aside>
        <main className="main">
          <Route path="/" component={homeScreen} exact={true} />
          <Route path="/productPage/:id" component={productPage} />
          <Route path="/cart/:id?" component={CartScreen} />
          <Route path="/shipping" component={ShippingScreen} />
          <Route path="/payment" component={PaymentMethod} />
          <Route path="/signin" component={signinScreen} />
          <Route path="/register" component={RegisterScreen} />
          <Route path="/products" component={ProductScreen} />
          <Route path="/placeorder" component={PlaceorderScreen} />
        </main>
        <footer className="footer">All rights received</footer>
      </div>
    </Router>
  );
}
